# Continuous Integration with Jenkins and Artifactory
Lab 1: Working with Ubuntu

---

## Exercise

Create new directory with the name "linux-lab"

```
mkdir linux-lab
```

Move to the directory
```
cd linux-lab
```

Clone the appliction repository from GitHub 
```
git clone https://github.com/sela-workshops/npm-demo-app.git
```

Move the application directory
```
cd npm-demo-app
```

Show the files on the directory
```
ls
```

Run "npm install" to download the node packages
```
npm install
```

Run "npm test" to run the application tests
```
npm test
```

Run "npm build" to build the application 
```
npm run-script build
```

Show all the files on the directoty in a list (you should see the file "nodejs-demoapp.zip")
```
ls -l
```

Run "npm start" to start the application 
```
npm start
```

Run "ifconfig" to see the linux network information
```
ifconfig
```

Run "ifconfig | grep inet" to see only the linux IP's network information
```
ifconfig | grep inet
```

Find the linux's machine IP and browse with port 3000 (for example: 192.168.2.44:3000)
You should see the web application

Press on Control + C to stop the application and exit
```
CTRL + C
```

Go back one directory
```
cd ..
```

Make a new directory with the name "web-app"
```
mkdir web-app
```

Move to the directory
```
cd web-app
```

Copy the zip file to this directory
```
cp ~/linux-lab/npm-demo-app/nodejs-demoapp.zip ~/linux-lab/web-app

```
Unzip the zip file
```
unzip nodejs-demoapp.zip
```

Show the files
```
ls
```

Open the "index.html" file in Vim
```
vim index.html
```

Press "i" to edit the file
```
ESC + i
```

Type some words

Press ESC to exit from edit mode
```
ESC
```

Run ":wq" to exit the file with save the changes
```
:wq
```

Run "cat primes.js" to see the content of the file
```
cat primes.js
```

Delete the file "nodejs-demoapp.zip"
```
rm nodejs-demoapp.zip
```

Go back to the root directory
```
cd ~
```

Remove the directory "linux-lab"
```
rm -rf linux-lab
```
